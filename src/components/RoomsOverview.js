import React, { useContext, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import ViewContext from '../context/ViewContext';
import RoomsContext from '../context/RoomsContext';
import { CircularProgress } from '@material-ui/core';
import Room from './Room';

export default function RoomViewer() {
    const { view, viewOptions } = useContext(ViewContext);
    const { setCurrentFloor, loading, rooms } = useContext(RoomsContext);
    const param = useParams();

    useEffect(() => {
        if(!loading && param.floorId !== undefined) {
            setCurrentFloor(parseInt(param.floorId));
        }
    });

    return(
        <div className={view === viewOptions[0] ? "MapView" : "ListView" }>
            {
            !loading ? rooms.map(room => <Room key={
                    room.id
                }
                room={room}/>) : <CircularProgress /> 
        } </div>
        
    )
}
