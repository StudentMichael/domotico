import React, {useEffect, useState} from 'react';
import {withStyles} from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import {useHistory} from 'react-router-dom';
import {useParams} from 'react-router-dom';
import Axios from 'axios';
import {DB_URL} from '../database/db';
import RoomSettingsInput from './RoomSettingsInput';
import Skeleton from '@material-ui/lab/Skeleton';


const styles = (theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(2)
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500]
    }
});

const DialogTitle = withStyles(styles)((props) => {
    const {
        children,
        classes,
        onClose,
        ...other
    } = props;
    return (
        <MuiDialogTitle disableTypography
            className={
                classes.root
            }
            {...other}>
            <Typography variant="h6">
                {children}</Typography>
            {
            onClose ? (
                <IconButton aria-label="close"
                    className={
                        classes.closeButton
                    }
                    onClick={onClose}>
                    <CloseIcon/>
                </IconButton>
            ) : null
        } </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2)
    }
}))(MuiDialogContent);


export default function RoomSettings() {
    const history = useHistory();
    const [roomDetailsLoaded, setRoomDetailsLoaded] = useState(false);
    const [room, setRoom] = useState();
    const param = useParams();

    const getRoom = async (id) => {
        const call = await Axios.get(`${DB_URL}/rooms/${param.roomId}`);

        setRoom(call.data);
    }

    const [open, setOpen] = React.useState(false);

    const handleClose = () => {
        history.push(`/floor/${param.floorId}/`)
        setOpen(false);
    };

    useEffect(() => {
        setOpen(true);
        getRoom();     
        //To give it an instagram-casino-feel, arbitrary wait
        const timer = setTimeout(() => {
          setRoomDetailsLoaded(true);
        }, 700);
        return () => {
          clearTimeout(timer);
        }
        // Geen dependencies, enkel bij init
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div style={
            {position: "relative"}
        }>
            <Dialog onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}>
                <DialogTitle id="customized-dialog-title"
                    onClose={handleClose}>
                    Edit room
                </DialogTitle>
                <DialogContent> {!roomDetailsLoaded ? 
                    <Skeleton variant="rect"
                      width={200}
                      height={400}/> 
                  : <RoomSettingsInput room={room}/>
                } 
                </DialogContent>
            </Dialog>
        </div>
    );
}
