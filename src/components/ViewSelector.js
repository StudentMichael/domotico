import React, {useContext, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import ViewContext from '../context/ViewContext';

const useStyles = makeStyles((theme) => ({
  root: {
    
  },
  ListItemText: {
      color:'red'
  }
}));


export default function ViewSelector() {
    const {setView, viewOptions} = useContext(ViewContext);

    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [selectedIndex, setSelectedIndex] = React.useState(0);

    useEffect(() => {
        setSelectedIndex(0)
    },[]);
    
    const handleClickListItem = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleMenuItemClick = (index, option, setView) => {
        setSelectedIndex(index);
        setAnchorEl(null);
        setView(option);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div className={classes.root}>
            <List component="nav" aria-label="View selector">
                <ListItem
                    button
                    aria-haspopup="true"
                    aria-controls="lock-menu"
                    aria-label="Selected view"
                    onClick={handleClickListItem}
                >
                <ListItemText primary="Selected view" secondary={viewOptions[selectedIndex]} secondaryTypographyProps={{color:'secondary'}} />
                </ListItem>
            </List>
            <Menu
                id="lock-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                {viewOptions.map((option, index) => (
                <MenuItem
                    key={option}
                    disabled={index === selectedIndex}
                    selected={index === selectedIndex}
                    onClick={() => handleMenuItemClick(index, option, setView)}
                >
                    {option}
                </MenuItem>
                ))}
            </Menu>
        </div>
    );
}
