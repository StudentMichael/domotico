import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import AddRoom from './AddRoom';
import FloorSelector from './FloorSelector';

const useStyles = makeStyles((theme) => ({
  text: {
    padding: theme.spacing(2, 2, 0),
  },
  appBar: {
    top: 'auto',
    bottom: 0,
    width: '100%',
    position: 'fixed',
  }

}));


export default function BottomAppBar() {
  const classes = useStyles();  

  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="fixed" color="primary" className={classes.appBar}>
        <Toolbar>
          <AddRoom />
          <FloorSelector />
        </Toolbar>
      </AppBar>
    </React.Fragment>
  );
}
