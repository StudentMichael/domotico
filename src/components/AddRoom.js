import React, {useState} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import AddRoomForm from './AddRoomInput';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  fabButton: {
    padding:100
  }
  
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);


export default function AddRoomDialog() {   
    const roomName = useTextFieldInput("Name");
    const width = useTextFieldInput("Width");
    const depth = useTextFieldInput("Depth");
    const x = useTextFieldInput("X");
    const y = useTextFieldInput("Y");

    function useTextFieldInput(name) {
        const [value, setValue] = useState("");
        function handleChange(e) {
            setValue(e.target.value);
    }
        return {
            value,
            label:name,
            onChange:handleChange
        }
    }

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div style={{position:"relative"}}>
      <Fab color="secondary" aria-label="add" className={styles.fabButton}  >
        <AddIcon onClick={handleClickOpen} />
      </Fab>
      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Add a new room...
        </DialogTitle>
        <DialogContent>
          <AddRoomForm roomName={roomName} width={width} depth={depth} x={x} y={y} handleClose={handleClose} />
        </DialogContent>
      </Dialog>
    </div>
  );
}
