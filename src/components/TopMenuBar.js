import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import ViewSelector from './ViewSelector';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    title: {
      flexGrow: 1,
      position: 'absolute',
      left: 0,
      right: 0,
      margin: '0 auto',
    }
}));
  
  export default function ButtonAppBar(props) {
    const classes = useStyles();
  
    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" className={classes.title}>
              DomuBueno
            </Typography>
            <ViewSelector />
          </Toolbar>
        </AppBar>
      </div>
    );
}  
