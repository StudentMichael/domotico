import React, {useContext} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import AddIcon from '@material-ui/icons/Add';
import FilterNoneIcon from '@material-ui/icons/FilterNone';
import RoomsContext from '../context/RoomsContext';
import { useHistory } from 'react-router-dom';
import {floorRoute} from '../routes';

const useStyles = makeStyles((theme) => ({
  root: {
    transform: 'translateZ(0px)',
    flexGrow: 1,
  },
  speedDial: {
    position: 'absolute',
    '&.MuiSpeedDial-directionUp, &.MuiSpeedDial-directionLeft': {
      bottom: theme.spacing(2),
      right: theme.spacing(2),
    },
  },
}));


export default function FloorSelector() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const { floors, addFloor } = useContext(RoomsContext);
  const history = useHistory();

  const handleClose = async(e, operation, floor) => {
     if(operation === 'floorChange'){
       history.push(`${floorRoute}/${floor}`);
     } else if(operation ==='addFloor') {
      addFloor();
     }

   setOpen(false);
 };
  const actions = [
    { icon: <AddIcon />, name: 'Add', className:'addButton', action:handleClose, operation:'addFloor' },
  ];

  for(const floor in floors) {
    let num = parseInt(floor,10)+1;
    actions.push({ icon: num, name: num, action: handleClose, operation: 'floorChange'})
  }


  const handleOpen = () => {
    setOpen(true);
  };

  return (
    <div className={classes.root}>

      <div className={classes.exampleWrapper}>
        <SpeedDial
          ariaLabel="Floor selector"
          className={classes.speedDial}
          icon={<FilterNoneIcon />}
          onClose={handleClose}
          onOpen={handleOpen}
          open={open}
        >
          {actions.map((action) => (
            <SpeedDialAction
              key={action.name}
              icon={action.icon}
              tooltipTitle={action.name}
              onClick={(e) => {
                handleClose(e, action.operation, action.name)
              }}
            />
          ))}
        </SpeedDial>
      </div>
    </div>
  );
}
