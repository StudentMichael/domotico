import { yellow } from '@material-ui/core/colors';
import React, { useContext } from 'react';
import VolumeOffIcon from '@material-ui/icons/VolumeOff';
import VolumeDownIcon from '@material-ui/icons/VolumeDown';
import VolumeUpIcon from '@material-ui/icons/VolumeUp';
import { useHistory } from 'react-router-dom';
import ViewContext from '../context/ViewContext';

export default function Room({room}) {
    const {view} = useContext(ViewContext);
    const primaryColor = room.curtainsOpen ? 'white' : 'black';
    const secondaryColor = room.curtainsOpen ? 'black' : 'white';
    //Round to highest 100, hue only supports values of 100
    const yellowValue = Math.ceil((room.lighting) / 4) * 100;
    const history = useHistory();

    let cssWidth, cssHeight;
    if(view === "Map view") {
        cssWidth=room.width;
        cssHeight=room.height;

    } else {
        cssWidth='80%';
        cssHeight='200px';
    }

    const myStyle = {
        width:cssWidth,
        height:cssHeight,
        left:room.x,
        top:room.y,
        backgroundColor:(room.lighting === 0 ? primaryColor : yellow[yellowValue]),
        color:secondaryColor,
        margin:'20px'
    }

    return(
        <div style={myStyle} className="MapSingleRoom" onClick={() => history.push(`/floor/${room.floor}/room/${room.id}`)}>
            {room.hasOwnProperty('temperature') && <p>{room.temperature}°C</p>}
            {
                room.hasOwnProperty('music') ? (
                    room.music === 0 ? <VolumeOffIcon color='primary' /> : (room.music < 10 ? <VolumeDownIcon color='primary'/> : <VolumeUpIcon color='primary'/>)
                )
                :<VolumeOffIcon color = 'primary'/>
            }
        </div>
    );
}