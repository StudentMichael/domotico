import React, {useState, useContext} from 'react';
import TextField from '@material-ui/core/TextField';
import Axios from 'axios';
import {DB_URL} from '../database/db'
import { Checkbox, FormControlLabel, Slider } from '@material-ui/core';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import RoomsContext from '../context/RoomsContext';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
    root: {
      transform: 'translateZ(0px)',
      flexGrow: 1,
      width: 200
    }
  }));

  

export default function RoomSettingsInput(room) {
    const classes = useStyles();
    const {refresh} = useContext(RoomsContext);
    const stateName = useTextFieldInput("name", "text");
    const stateDescription = useTextFieldInput("description", "text");
    const stateX = useTextFieldInput("x", "number");
    const stateY = useTextFieldInput("y", "number");
    const stateWidth = useTextFieldInput("width", "number");
    const stateDepth = useTextFieldInput("height", "number");

    const stateTemperature = useSlider("temperature", 0, 30);
    const stateLighting = useSlider("lighting", 0, 20);
    const stateMusic = useSlider("music", 0, 20);
    const stateCurtains = useCheckbox("curtainsOpen");
    room = room.room;

    function useTextFieldInput(name, type) {
        const [value, setValue] = useState();
        function handleChange(e) {
            let valueToSet;
            if(type === "number")
            {
                valueToSet = parseInt(e.target.value);
                if(valueToSet > 500)
                    valueToSet = 500;
                if(valueToSet < 0 || isNaN(valueToSet))
                    valueToSet = 0;
            } else {
                valueToSet = e.target.value;
            } 
            setValue(valueToSet);
            axiosPatch(name,valueToSet);
        }
        
        return {value, label: name, onChange: handleChange}
    }

    function useSlider(name, min, max) {
        const [value, setValue] = useState();
        function handleChange(e, value) {
            const valueToSet = parseInt(value);
            setValue(valueToSet);
            axiosPatch(name,valueToSet);
        }
        return {value, label: name, onChangeCommitted: handleChange, min, max}
    }

    function useCheckbox(name) {
        const [value, setValue] = useState();
        function handleChange(e) {
            setValue(e.target.checked);
            axiosPatch(name,e.target.checked);
        }
        return { value, label:name, onChange:handleChange }
    }

    const clone = (obj) => Object.assign({}, obj);

    async function axiosPatch(name,value) {
        const obj={name:value};  
        const clonedObj = clone(obj);
        const targetKey = clonedObj["name"];
        delete clonedObj["name"];
        clonedObj[name] = targetKey;
        const response = await Axios.patch(`${DB_URL}/rooms/${room.id}`,clonedObj);
        console.log(response);
        refresh();
    }

    return(
        <div className={classes.root}>
            <TextField {...stateName} defaultValue={room.name} fullWidth/>
            <TextField {...stateDescription} defaultValue={room.description} 
                multiline rows="3"
                margin="normal"
                variant="outlined"
                fullWidth
            />
            <TextField {...stateX} defaultValue={room.x}
            />
            <TextField {...stateY} defaultValue={room.y}/>
            <TextField {...stateWidth} defaultValue={room.width}/>
            <TextField {...stateDepth} defaultValue={room.height}/>
            Temperature
            <Slider {...stateTemperature} className={classes.slider} defaultValue={room.temperature} valueLabelDisplay="auto" width="50" />
            Light
            <Slider {...stateLighting} className={classes.slider} defaultValue={room.lighting} valueLabelDisplay="auto" width="100%"/>
            Music
            <Slider {...stateMusic} className={classes.slider} defaultValue={room.music} valueLabelDisplay="auto" width="100%"/>
            <FormControlLabel
                control={
                <Checkbox
                    {...stateCurtains}
                    defaultChecked={room.curtainsOpen}
                    icon={<CheckBoxOutlineBlankIcon fontSize="small" />}
                    checkedIcon={<CheckBoxIcon fontSize="small" />}
                    name="checkedI"
                />
                }
                label="Curtains open?"
            />
        </div>
    )
}
