import React, {useContext} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import RoomsContext from '../context/RoomsContext';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        display: 'block',
    },
    button: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },

  }));  
  
export default function HorizontalLinearStepper({roomName,width,depth,x,y, handleClose}) {
  
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  let response = "success";
  const steps = getSteps();
  const {addRoom} = useContext(RoomsContext);

  function getSteps() {
    return ['Room name', 'Size', 'Position'];
  }

    function getStepContent(step) {
      switch (step) {
        case 0:
          return (<TextField {...roomName} />);
        case 1:
            return (
                <>
                    <TextField {...width} type="Number" 
                        inputProps={{ min: "0", max: "100", step: "1" }}
                      onInput = {(e) =>{
                        e.target.value = Math.max(0, parseInt(e.target.value) ).toString().slice(0,12)
                    }} />
                    <br/>
                    <TextField {...depth} type="Number"
                        inputProps={{ min: "0", max: "100", step: "1" }}
                      onInput = {(e) =>{
                        e.target.value = Math.max(0, parseInt(e.target.value) ).toString().slice(0,12)
                    }} />
                </>
                );
        case 2:
          return (
            <>
                <TextField {...x} type="Number"
                        inputProps={{ min: "0", max: "600", step: "1" }}
                      onInput = {(e) =>{
                        e.target.value = Math.max(0, parseInt(e.target.value) ).toString().slice(0,12)
                    }} />
                <br/>
                <TextField {...y} type="Number"
                        inputProps={{ min: "0", max: "600", step: "1" }}
                      onInput = {(e) =>{
                        e.target.value = Math.max(0, parseInt(e.target.value) ).toString().slice(0,12)
                    }}/>
            </>
            );
        default:
          return 'Unknown step';
      }
    }
  
    const handleNext = () => {  
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };
  
    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };
  
    const handleReset = () => {
        setActiveStep(0);
      };

    const handleSave = async() => {
      response = await addRoom(roomName.value, x.value, y.value, width.value, depth.value);
      console.log(response);
      if(response === "success") {
        handleClose();
      }
    };
  
    return (
      <div className={classes.root}>
        <Stepper activeStep={activeStep}>
          {steps.map((label, index) => {
            const stepProps = {};
            const labelProps = {};

            return (
              <Step key={label} {...stepProps}>
                <StepLabel {...labelProps}>{label}</StepLabel>
              </Step>
            );
          })}
        </Stepper>
        <div>
          {activeStep === steps.length ? (
            <div>
              <Typography className={classes.instructions}>
                All steps completed - would you like to save this room or start over?
                {response !== "success" && <h1>Error during posting to database: {response}</h1>}
              </Typography>
              <Button onClick={handleSave} className={classes.button}>
                Save
              </Button>

              <Button onClick={handleReset} className={classes.button}>
                Reset
              </Button>
            </div>
          ) : (
            <div>
              <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
              <div>
                <Button disabled={activeStep === 0} onClick={handleBack} className={classes.button}>
                  Back
                </Button>  
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleNext}
                  className={classes.button}
                >
                  {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                </Button>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }