import './App.css';
import React from 'react';
import TopMenuBar from './components/TopMenuBar';
import BottomMenuBar from './components/BottomMenuBar';
import ViewProvider from './context/ViewProvider';
import RoomsProvider from './context/RoomsProvider';
import RoomsOverview from './components/RoomsOverview';
import RoomDetails from './components/RoomSettings';
import {homeRoute, floorRoute} from './routes';
import {BrowserRouter, Route} from 'react-router-dom';

 function App() {
  return (
    <div className="App">
      <ViewProvider>
        <TopMenuBar/>
        <RoomsProvider>
          <BrowserRouter>
            <Route exact path={homeRoute}>
                <h1>Welcome home, please select a floor to regulate.</h1>
            </Route>
            <Route path={`${floorRoute}/:floorId`}>
                <RoomsOverview/>
            </Route>
            <Route path={`${floorRoute}/:floorId/room/:roomId`}>
              <RoomDetails />
            </Route>
            <BottomMenuBar />
          </BrowserRouter>
        </RoomsProvider>
      </ViewProvider>
    </div>
  );
}

export default App;
