import Axios from 'axios';
import {useEffect, useState} from 'react';
import {DB_URL} from '../database/db';

export default function useAllRooms() {
    const [floors,setFloors] = useState();
    const [rooms,setRooms] = useState([]);
    const [loading, setLoading] = useState(true);
    const [currentFloor, setCurrentFloor] = useState();
    
    async function getFloors() {
        const getData = await Axios.get(`${DB_URL}/config`);
        setFloors(getData.data.floors);
    }

    async function addFloor() {
        const newFloors = [...floors,floors.length+1]
        setFloors(newFloors);
        const resp = await Axios.put(`${DB_URL}/config`, {floors:newFloors});
        console.log(resp.data);
    }

    async function getFloorRooms() {
        const getData = await Axios.get(`${DB_URL}/rooms`);
        const data = getData.data;
        
        const roomsOnFloor = data.filter(function(room) {
            return room.floor === currentFloor
        });
        setRooms(roomsOnFloor);
    }

    async function addRoom(roomName, x, y, width, height) {
        const getData = await Axios.get(`${DB_URL}/rooms`);
        console.log(getData.data);
        let maxId = getData.data.reduce(
            (max, room) => (room.id > max ? room.id : max),
            rooms[0].id
          );
          console.log(rooms);
        
        Axios.post(`${DB_URL}/rooms`, {
            id:maxId + 1,
            floor:currentFloor,
            name:roomName,
            x:parseInt(x, 10),
            y:parseInt(y,10),
            width:parseInt(width,10),
            height:parseInt(height,10)
        })
        .then(function(response) {
            console.log(response);
            refresh();
            return "success";
        })
        .catch(function(error) {
            console.log(error);
            return error.message;
        })
    }

    function refresh() {
        setLoading(true);
        getFloors();
        getFloorRooms();
        setLoading(false);
    }

    useEffect(() => {
        refresh();
        // eslint-disable-next-line
    },[currentFloor]);

    return {floors, rooms, loading, refresh, addRoom, setCurrentFloor, addFloor};
}
