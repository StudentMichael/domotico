import {useState} from 'react';

export default function useView() {
    const [view, setView] = useState("Map view");

    const viewOptions = [
      "Map view",
      "List view"
    ];
    
    return{view, setView, viewOptions};
}