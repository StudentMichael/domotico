import React from 'react';
import ViewContext from './ViewContext';
import useView from '../hooks/useView';

export default function RoomsProvider({children}) {
    const {view, setView, viewOptions} = useView();
    
    
    return(
        <ViewContext.Provider value={{
            view,
            setView,
            viewOptions
        }}>
            {children}
        </ViewContext.Provider>
    )
}