import React from 'react';
import RoomsContext from './RoomsContext';
import useAllRooms from '../hooks/useAllRooms';

export default function RoomsProvider({children}) {
    const {floors, rooms, loading, setCurrentFloor, addRoom, addFloor, refresh} = useAllRooms();
    return(
        <RoomsContext.Provider value={{
            floors,
            rooms,
            loading,
            setCurrentFloor,
            addRoom, 
            addFloor,
            refresh,
        }}>
            {children}
        </RoomsContext.Provider>
    )
}