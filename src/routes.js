export const homeRoute = "/";
export const floorRoute = "/floor";
export const addRoomRoute = "/floor/:id/add";